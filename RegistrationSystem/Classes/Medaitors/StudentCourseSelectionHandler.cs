﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RegistrationSystem.Interfaces;
using RegistrationSystem.Models.ViewModels;

namespace RegistrationSystem.Classes.Medaitors
{
    public class StudentCourseSelectionHandler : IRequestHandler<StudentCourseSelectionRequest, StudentCourseSelecionResponse>
    {
        private readonly IRegistrationService _registrationService;

        public StudentCourseSelectionHandler(IRegistrationService registrationService)
        {
            _registrationService = registrationService;
        }

        public async Task<StudentCourseSelecionResponse> Handle(StudentCourseSelectionRequest request, CancellationToken cancellationToken)
        {
            var response = new StudentCourseSelecionResponse
            {
                IsSuccess = await _registrationService.HandleRegistration(request)
            };

            return response;
        }
    }
}
