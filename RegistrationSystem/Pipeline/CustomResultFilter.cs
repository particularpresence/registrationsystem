﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;

namespace RegistrationSystem.Pipeline
{
    public class CustomResultFilter : IResultFilter
    {
        public void OnResultExecuting(ResultExecutingContext context)
        {
            var response = context.Result;

            Debug.WriteLine(response);

        }

        public void OnResultExecuted(ResultExecutedContext context)
        {
            var response = context.Result;

            Debug.WriteLine(response);
        }
    }
}
