﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using RegistrationSystem.Data;

namespace RegistrationSystem.Pipeline
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly SignInManager<IdentityUser> _signInManager;

        public ExceptionFilter(ApplicationDbContext dbContext, ILogger<ExceptionFilter> logger, IHostingEnvironment hostingEnvironment, SignInManager<IdentityUser> signInManager)
        {
            _dbContext = dbContext;
            _hostingEnvironment = hostingEnvironment;
            _signInManager = signInManager;
        }

        public override async void OnException(ExceptionContext exceptionContext)
        {
            if (!_hostingEnvironment.IsDevelopment())
            {
                exceptionContext.ExceptionHandled = false;
                return;
            }

            var exception = exceptionContext.Exception;

            if (exception is CryptographicException)
            {
                exceptionContext.Result = new RedirectToActionResult("Error","Home", new { });
            }

            if (exception is UnauthorizedAccessException)
            {
                await _signInManager.SignOutAsync();

                exceptionContext.Result = new RedirectResult("/Login");
            }

        }
    }
}
