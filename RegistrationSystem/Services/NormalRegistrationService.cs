﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.EntityFrameworkCore;
using RegistrationSystem.Classes.Contants;
using RegistrationSystem.Data;
using RegistrationSystem.Interfaces;
using RegistrationSystem.Models;
using RegistrationSystem.Models.ViewModels;

namespace RegistrationSystem.Services
{
    public class NormalRegistrationService : IRegistrationService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IDataProtector _protector;

        public NormalRegistrationService(IDataProtector protector, ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _protector = protector;
        }
        public async Task<bool> HandleRegistration(StudentCourseSelectionRequest request)
        {
            Student student = _dbContext.Students.Include(s => s.Courses).First(s => s.Id == int.Parse(_protector.Unprotect(request.StudentId)));
            var modified = false;
            foreach (int courseId in request.CourseIds.Select(protectedCourseId => int.Parse(_protector.Unprotect(protectedCourseId))).Where(courseId => student.Courses.All(c => c.CourseId != courseId)))
            {
                modified = true;
                student.Courses.Add(new StudentCourse { CourseId = courseId });
            }

            if (!modified)
                throw new InvalidOperationException($"No new courses were provided for {student.Name}");

            _dbContext.Update(student);
            var result = await _dbContext.SaveChangesAsync();

            return result > 0;

        }
    }
}
