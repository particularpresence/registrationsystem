﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RegistrationSystem.Models.ViewModels;

namespace RegistrationSystem.Interfaces
{
    public interface IRegistrationService
    {
        Task<bool> HandleRegistration(StudentCourseSelectionRequest request);
    }
}
