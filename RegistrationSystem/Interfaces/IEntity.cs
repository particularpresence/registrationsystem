﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationSystem.Interfaces
{
    public interface IEntity
    {
        int? Id { get; set; }
        DateTime? DateCreated { get; set; }
        DateTime? DateModified { get; set; }
        string CreatedByUser { get; set; }
        string ModifiedByUser { get; set; }
    }
}
