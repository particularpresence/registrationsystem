﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RegistrationSystem.Classes.Contants;
using RegistrationSystem.Data;
using RegistrationSystem.Models;
using RegistrationSystem.Models.ViewModels;

namespace RegistrationSystem.Controllers
{
    public class StudentsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMediator _mediator;
        private readonly IDataProtector _protector;

        public StudentsController(ApplicationDbContext context, IMediator mediator, IDataProtector protector)
        {
            _context = context;
            _mediator = mediator;
            _protector = protector;
        }

        // GET: Students
        public async Task<IActionResult> Index()
        {
            return View(await _context.Students.Include(student => student.Courses).ThenInclude(studentCourse => studentCourse.Course).ToListAsync());
        }

        // GET: Students/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .FirstOrDefaultAsync(m => m.Id == int.Parse(_protector.Unprotect(id)));
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // GET: Students/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Id")] Student student)
        {
            if (ModelState.IsValid)
            {
                _context.Add(student);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        // GET: Students/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, [Bind("Name,Id")] Student student)
        {
            if (id != student.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(student);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentExists(student.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        // GET: Students/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .FirstOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            var student = await _context.Students.FindAsync(id);
            _context.Students.Remove(student);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StudentExists(int? id)
        {
            return _context.Students.Any(e => e.Id == id);
        }

        public async Task<IActionResult> RegisterForCourses()
        {
            var students = _context.Students.ToList();
            students.ForEach(s => s.DataProtector = _protector);
            ViewData["Students"] = new SelectList(students, "ProtectedId", "Name");



            var courses = _context.Courses.ToList();
            courses.ForEach(s => s.DataProtector = _protector);
            ViewData["Courses"] = new SelectList(courses, "ProtectedId", "Name");

            return View();
        }
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> RegisterForCourses(StudentCourseSelectionRequest courseSelectionRequest)
        {
            if (ModelState.IsValid)
            {
                var response = await _mediator.Send(courseSelectionRequest);

                if (response.IsSuccess)
                {
                    RedirectToAction("Details", "Students", new { id = courseSelectionRequest.StudentId });
                }
            }

            return View(courseSelectionRequest);
        }
    }
}
