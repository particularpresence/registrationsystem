﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RegistrationSystem.Interfaces;
using RegistrationSystem.Models;

namespace RegistrationSystem.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        private readonly IHttpContextAccessor _iHttpContextAccessor;
        private readonly UserManager<IdentityUser> _userManager;



        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IHttpContextAccessor iHttpContextAccessor)
            : base(options)
        {
            _iHttpContextAccessor = iHttpContextAccessor;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<StudentCourse>().HasKey(sc => new { sc.CourseId, sc.StudentId });

            base.OnModelCreating(builder);
        }


        private void SetAuditFields()
        {
            var entriesToSave = ChangeTracker.Entries<IEntity>();
            var user = _iHttpContextAccessor.HttpContext.User;

            foreach (var entityEntry in entriesToSave.Where(entry => entry.State == EntityState.Added))
            {
                entityEntry.Entity.DateCreated = DateTime.Now;
                if (user != null)
                    entityEntry.Entity.CreatedByUser = user.Identity.Name;
            }

            foreach (var entityEntry in entriesToSave.Where(entry => entry.State == EntityState.Modified))
            {
                entityEntry.Entity.DateModified = DateTime.Now;
                if (user != null)
                    entityEntry.Entity.ModifiedByUser = user.Identity.Name;
            }
        }

        public override int SaveChanges()
        {
            SetAuditFields();

            return base.SaveChanges();
        }


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SetAuditFields();

            return base.SaveChangesAsync(cancellationToken);
        }


        public DbSet<RegistrationSystem.Models.Course> Courses { get; set; }

        public DbSet<RegistrationSystem.Models.Student> Students { get; set; }

        public DbSet<RegistrationSystem.Models.Tutor> Tutors { get; set; }
    }
}
