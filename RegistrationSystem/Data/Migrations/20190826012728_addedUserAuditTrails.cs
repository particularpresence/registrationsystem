﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RegistrationSystem.Data.Migrations
{
    public partial class addedUserAuditTrails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreatedByUser",
                table: "Tutors",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ModifiedByUser",
                table: "Tutors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedByUser",
                table: "Students",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ModifiedByUser",
                table: "Students",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedByUser",
                table: "Courses",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ModifiedByUser",
                table: "Courses",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedByUser",
                table: "Tutors");

            migrationBuilder.DropColumn(
                name: "ModifiedByUser",
                table: "Tutors");

            migrationBuilder.DropColumn(
                name: "CreatedByUser",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "ModifiedByUser",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "CreatedByUser",
                table: "Courses");

            migrationBuilder.DropColumn(
                name: "ModifiedByUser",
                table: "Courses");
        }
    }
}
