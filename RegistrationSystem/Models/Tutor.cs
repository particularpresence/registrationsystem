﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace RegistrationSystem.Models
{
    public class Tutor : BaseEntity
    {
        public string Name { get; set; }

        public int? CourseId { get; set; }
        public virtual Course Course { get; set; }
    }
}
