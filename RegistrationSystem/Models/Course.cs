﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RegistrationSystem.Models
{
    public class Course : BaseEntity
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }

        [NotMapped]
        public string DisplayName => $"{Code} - {Name}";
        public virtual ICollection<Tutor> Tutors { get; set; } = new List<Tutor>();
        public virtual ICollection<StudentCourse> Students { get; set; } = new List<StudentCourse>();

        [NotMapped,DisplayName("# Registered")]
        public int NumberOfRegisteredStudents => Students.Count();
    }
}
