﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using RegistrationSystem.Classes.Medaitors;

namespace RegistrationSystem.Models.ViewModels
{
    public class StudentCourseSelectionRequest : IRequest<StudentCourseSelecionResponse>
    {
        [Required]
        public string StudentId { get; set; }
        public List<string> CourseIds { get; set; }
    }
}
