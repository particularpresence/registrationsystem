﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.DataProtection;
using RegistrationSystem.Interfaces;

namespace RegistrationSystem.Models
{
    public abstract class BaseEntity : IEntity
    {
        [Key]
        public int? Id { get; set; }

        [Required, ScaffoldColumn(false)]
        public DateTime? DateCreated { get; set; } = DateTime.Now;

        [ScaffoldColumn(false)]
        public DateTime? DateModified { get; set; }

        public string CreatedByUser { get; set; }

        public string ModifiedByUser { get; set; }

        [NotMapped]
        public IDataProtector DataProtector { get; set; } = null;

        [NotMapped]
        public string ProtectedId =>
            DataProtector == null
                ? string.Empty
                : DataProtector.Protect(Id.ToString());

    }
}
